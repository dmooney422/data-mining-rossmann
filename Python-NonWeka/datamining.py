
import pandas as pd
import numpy as np
from pandas import Series, DataFrame, Panel
import time, sys

def main():
	ao = np.loadtxt('monthly.ao.index.b50.current.ascii')

	dates = pd.date_range('1950-01', '2015-11', freq='M')
	a = Series(ao[:,2], index=dates)
	a.plot()
	input("prompt: ")

if __name__ == "__main__":
	main()
	