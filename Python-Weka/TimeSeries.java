package weka.classifiers.timeseries;

import java.io.*;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import weka.core.Instances;

import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.timeseries.WekaForecaster;
import weka.classifiers.timeseries.core.TSLagMaker;

import weka.filters.Filter;

import weka.filters.unsupervised.instance.RemoveWithValues;

/**
 * Example of using the time series forecasting API. To compile and run the
 * CLASSPATH will need to contain:
 * 
 * weka.jar (from your weka distribution)
 * pdm-timeseriesforecasting-ce-TRUNK-SNAPSHOT.jar (from the time series
 * package) jcommon-1.0.14.jar (from the time series package lib directory)
 * jfreechart-1.0.13.jar (from the time series package lib directory)
 */

public class TimeSeriesExample {

	protected static String m_skipEntries;

	public static void main(String[] args) {

		File path = new File("C:/Users/ShkelqimG/Desktop/artff files/");

		String[] files = path.list();

		for (int i = 0; i < files.length; i++)

		{ // System.out.println(path + "/" + files[i]);
			System.out.println(files[i]);
			// File read = new File(path + "/" + files[i]);
			// String[] fileNames = read.list();
			// for (int j = 0; j < files.length; j++) {

			try {
				// path to the Australian wine data included with the time
				// series forecasting
				// package
				String pathToWineData = path + "/" + files[i];

				// load the wine data
				Instances wine = new Instances(new BufferedReader(
						new FileReader(pathToWineData)));

				// new forecaster
				WekaForecaster forecaster = new WekaForecaster();

				RemoveWithValues filter = new RemoveWithValues();

				String[] options = new String[4];
				options[0] = "-C"; // attribute index
				options[1] = "5"; // 5
				options[2] = "-S"; // match if value is smaller than
				options[3] = "1"; // 1
				filter.setOptions(options);

				// String m_skipEntries;
				// PeriodicityHandler periodicityHandler;
				//
				//
				// if (m_skipEntries != null && m_skipEntries.length() > 0)
				// {
				// try {
				// periodicityHandler.setSkipList(m_skipEntries,
				// "SUN, 12/26@MM/dd,12/25@MM/dd,4/1@MM/dd, 3/29@MM/dd,10/3@MM/dd,5/30@MM/dd,5/20@MM/dd,5/9@MM/dd,5/1@MM/dd,1/1@MM/dd");
				// } catch (Exception ex) {
				// ex.printStackTrace();
				// }

				filter.setInputFormat(wine);
				Instances newData = Filter.useFilter(wine, filter);

				// set the targets we want to forecast. This method calls
				// setFieldsToLag() on the lag maker object for us
				forecaster.setFieldsToForecast("Sales");

				// default underlying classifier is SMOreg (SVM) - we'll use
				// gaussian processes for regression instead

				forecaster.setBaseForecaster(new LinearRegression());

				forecaster.getTSLagMaker().setTimeStampField("Date"); // date
																		// time
																		// stamp
				forecaster.getTSLagMaker().setMinLag(1);
				forecaster.getTSLagMaker().setMaxLag(7); // daily data

				// add a month of the year indicator field
        	//	forecaster.getTSLagMaker().setAddMonthOfYear(true);
//
//				// add a quarter of the year indicator field
//				forecaster.getTSLagMaker().setAddQuarterOfYear(true);
			
				forecaster.getTSLagMaker().setAddDayOfMonth(true);
			//	forecaster.getTSLagMaker().setAddDayOfWeek(true);

				// setOptions(new String[] { "-T", "date" });

				String skipString = "SUN, 12/26@MM/dd,12/25@MM/dd,4/1@MM/dd, 3/29@MM/dd,10/3@MM/dd,5/30@MM/dd,5/20@MM/dd,5/9@MM/dd,5/1@MM/dd,1/1@MM/dd";
				// Utils.getOption("skip", options2);

				forecaster.getTSLagMaker().setSkipEntries(skipString);

				forecaster.getTSLagMaker().setPeriodicity(
						TSLagMaker.Periodicity.DAILY);

				// build the model
				forecaster.buildForecaster(newData, System.out);

				// prime the forecaster with enough recent historical data
				// to cover up to the maximum lag. In our case, we could
				// just
				// supply
				// the 12 most recent historical instances, as this covers
				// our
				// maximum
				// lag period
				forecaster.primeForecaster(newData);

				// forecast for 42 days beyond the end of the
				// training data
				List<List<NumericPrediction>> forecast = forecaster.forecast(
						42, System.out);

				// output the predictions. Outer list is over the steps;
				// inner
				// list is over
				// the targets

				String dt = "2015-08-01"; // Start date
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(dt));
				System.out.println("Store     Date     Dollar Sales");
				System.out.println("-------------------------------");
				for (int i1 = 0; i1 < 42; i1++) {
					List<NumericPrediction> predsAtStep = forecast.get(i1);
					// for (int j = 0; j < 2; j++) {
					NumericPrediction predForTarget = predsAtStep.get(0);

					c.add(Calendar.DATE, 1); // number of days to add
					dt = sdf.format(c.getTime()); // dt is now the new date

					System.out.print(i+1 + "    " + dt + "    "
							+ predForTarget.predicted() + " ");
					// }
					System.out.println();
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
