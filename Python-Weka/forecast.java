import java.io.*;
import java.util.List;
import weka.core.Instances;
import weka.classifiers.functions.GaussianProcesses;
import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.timeseries.WekaForecaster;
import weka.classifiers.timeseries.core.TSLagMaker;

//This forecast class builds a forecast from the file argument passed

//Things I need to add
//Skip list and make sure matches Weka output
//Output test measures
 

public class forecast {

  public static void main(String[] args) {
    try {
      
      String pathToTrainingData = args[0];	
      Instances train = new Instances(new BufferedReader(new FileReader(pathToTrainingData)));
      WekaForecaster forecaster = new WekaForecaster();

      forecaster.setFieldsToForecast("Sales");
      forecaster.setBaseForecaster(new GaussianProcesses());
      //forecaster.setSkipList("")
      forecaster.getTSLagMaker().setTimeStampField("Date");

      forecaster.buildForecaster(train, System.out);
      forecaster.primeForecaster(train);
      List<List<NumericPrediction>> forecast = forecaster.forecast(50, System.out);

      for (int i = 0; i < 50; i++) {
      	List<NumericPrediction> predsAtStep = forecast.get(i);
      	for (int j = 0; j < 1; j++) {
      		NumericPrediction predForTarget = predsAtStep.get(j);
      		System.out.print("" + predForTarget.predicted() + " ");
      	}
      System.out.println();
      }



    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}