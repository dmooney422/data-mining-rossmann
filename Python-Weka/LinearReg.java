package weka.classifiers.timeseries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.timeseries.core.TSLagMaker;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveWithValues;

public class LinearReg {

	public static void main(String[] args) throws Exception {

		// path to folder
		File path = new File("C:/Users/ShkelqimG/Desktop/artff files/");

		String[] files = path.list();
		int rowID = 1;
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);

		// output results
		PrintWriter writer = new PrintWriter("result.txt", "UTF-8");

		// promotion's of one month
		Integer[] promo1 = { 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
				0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1,
				1, 1, 1 };

		System.out.println(promo1.length);
		List<Integer> promo = Arrays.asList(promo1);

		writer.println("ID    Dollar Sales");
		writer.println("----------------------------");

		System.out.println("Forecasting in process... (will take around 10 minutes)");

		for (int i = 0; i < files.length; i++) {

			String pathToWineData = path + "/" + files[i];
			Instances instNew;
			double factor = 0;
			Remove remove;
			// load the wine data
			Instances inst = new Instances(new BufferedReader(new FileReader(
					pathToWineData)));

			RemoveWithValues filter = new RemoveWithValues();

			// remove open = 0
			String[] options1 = new String[4];
			options1[0] = "-C"; // attribute index
			options1[1] = "5"; // 5
			options1[2] = "-S"; // match if value is smaller than
			options1[3] = "1"; // 1
			filter.setOptions(options1);
			filter.setInputFormat(inst);
			Instances newData = Filter.useFilter(inst, filter);

			// remove Sundays
			String[] options3 = new String[4];
			options3[0] = "-C"; // attribute index
			options3[1] = "1"; // 1
			options3[2] = "-S"; // match if value is equal to
			options3[3] = "7"; // 7
			filter.setOptions(options3);
			filter.setInvertSelection(true);
			filter.setInputFormat(newData);
			Instances newData1 = Filter.useFilter(newData, filter);

			// remove all attributes other than sales and promo for finding
			// correlation between sales and promo
			String[] options = weka.core.Utils.splitOptions("-R 1-2");
			remove = new Remove();
			remove.setOptions(options);
			remove.setInputFormat(newData1);
			instNew = Filter.useFilter(newData1, remove);

			String[] options11 = weka.core.Utils.splitOptions("-R 2-3");
			remove = new Remove();
			remove.setOptions(options11);
			remove.setInputFormat(instNew);
			Instances instNew1 = Filter.useFilter(instNew, remove);

			String[] options12 = weka.core.Utils.splitOptions("-R 3-4");
			remove = new Remove();
			remove.setOptions(options12);
			remove.setInputFormat(instNew1);
			Instances instNew2 = Filter.useFilter(instNew1, remove);
			instNew2.setClassIndex(0);

			// Remove promo=0
			String[] options2 = new String[4];
			options2[0] = "-C"; // attribute index
			options2[1] = "2"; // 5
			options2[2] = "-S"; // match if value is smaller than
			options2[3] = "1"; // 1
			filter.setOptions(options2);
			filter.setInvertSelection(false);
			filter.setInputFormat(instNew2);
			// Instances noPromotion = Filter.useFilter(instNew2, filter);

			// Linear Regresion

			// build model
			LinearRegression model = new LinearRegression();
			model.buildClassifier(instNew2); // the last instance with missing
			double coefficients[] = model.coefficients();
			factor = coefficients[1];

			WekaForecaster forecaster = new WekaForecaster();

			// Forecast
			forecaster.setFieldsToForecast("Sales");

			forecaster.setBaseForecaster(new LinearRegression());

			forecaster.getTSLagMaker().setTimeStampField("Date"); // date
																	// time
																	// //
																	// stamp
			forecaster.getTSLagMaker().setMinLag(1);
			forecaster.getTSLagMaker().setMaxLag(7); // daily data

			forecaster.getTSLagMaker().setAddDayOfWeek(true);

			// skip list
			String skipString = "SUN, 12/26@MM/dd, 12/25@MM/dd,4/1@MM/dd, 3/29@MM/dd,10/3@MM/dd,5/30@MM/dd,5/20@MM/dd,5/9@MM/dd,5/1@MM/dd,1/1@MM/dd";

			forecaster.getTSLagMaker().setSkipEntries(skipString);

			forecaster.getTSLagMaker().setPeriodicity(
					TSLagMaker.Periodicity.DAILY);

			// build the model for forecasting
			forecaster.buildForecaster(newData1);
			forecaster.primeForecaster(newData1);

			// forecast for 41 days beyond the end of the
			// training data
			List<List<NumericPrediction>> forecast = forecaster.forecast(41,
					System.out);

			for (int i1 = 0; i1 < forecast.size(); i1++) {
				List<NumericPrediction> predsAtStep = forecast.get(i1);

				NumericPrediction predForTarget = predsAtStep.get(0);

				if (promo.get(i1) == 1) {

					double withPromo = predForTarget.predicted() + factor;
					writer.println(rowID + "   " + df.format(withPromo));
				} else {
					writer.println(rowID + "   "
							+ df.format(predForTarget.predicted()) + " ");
				}
				rowID++;
			}
		}
		writer.close();
		System.out.println("Forecasting complete!");
	}
}
