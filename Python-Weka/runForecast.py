#Picks up all the .arff files in path
#Sends them through the Java forecast program

import os
from subprocess import call

def main():
	path = 'stores/'
	listing = os.listdir(path)
	
	count = 0
	for file in listing:
		if file.endswith(".arff"):
			count = count + 1
			call(["java", "forecast",path+file])
			#print "printing forecast file" + path+file
			

if __name__ == '__main__':              
    main() 