import weka.core.jvm as jvm
from weka.core.converters import Loader
from weka.classifiers import Classifier


jvm.start()
loader = Loader(classname="weka.core.converters.ArffLoader")

data = loader.load_file("stores/store1.arff")

data.delete_attribute(0)
data.delete_attribute(0)
data.delete_attribute(1)
data.delete_attribute(1)
data.delete_attribute(2)
data.delete_attribute(2)


cls = Classifier(classname="weka.classifiers.functions.LinearRegression")
data.class_is_first()
cls.build_classifier(data)
print cls
