#http://pythonhosted.org//javabridge/highlevel.html#wrapping-java-objects-using-reflection
#http://wiki.pentaho.com/display/DATAMINING/Time+Series+Analysis+and+Forecasting+with+Weka


from javabridge import JWrapper
import javabridge
import weka.core.jvm as jvm
from weka.core.converters import Loader

def main():
	print 'starting JVM'
	jvm.start()
	forecaster = JWrapper(javabridge.make_instance("weka/classifiers/timeseries/WekaForecaster", "()V"))
	print 'ok'
	loader = Loader(classname="weka.core.converters.ArffLoader")
	data = loader.load_file("train.arff")
	
	#print forecaster
	#print dir(forecaster)
	
	#print "test"
	print type(data)
	forecaster.setFieldsToForecast("Sales")
	
	# need to figure system.out in python
	#print forecaster.buildForecaster.__getattr__
		

	
	jvm.stop()
	print 'ending  JVM'


if __name__ == "__main__":
	main()

